
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GameMouseListener implements MouseListener {
	GameMapPanel panel;
	
	GameMouseListener(GameMapPanel p) {
		panel = p;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		int squareX = (e.getX()) / 30;
		int squareY = (e.getY()) / 30;
		panel.karta.hero.posx = squareX;
		panel.karta.hero.posy = squareY;
		panel.repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}

