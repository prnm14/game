
public class Programa extends Object{
	public static void main(String[] args) throws InterruptedException {
		
		Karta k = new Karta(8, 8);
		Hero h = new Hero(0, 1);
		k.hero = h;
		
		GameFrame f = new GameFrame(k);
		
		f.setLayout(null);
		f.setSize(f.getMaximumSize());
		f.setVisible(true);
	}
	public String toString() {
		return "My game";
	}
}
