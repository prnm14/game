
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GameWindowListener implements WindowListener {
	GameFrame frame;
	
	GameWindowListener(GameFrame f) {
		frame = f;
	}

	public void windowOpened(WindowEvent e) {
		try {
			FileInputStream fis = new FileInputStream("karta.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			frame.mapPanel.karta = (Karta)ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException | ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	public void windowClosing(WindowEvent e) {
		try {
			FileOutputStream fos = new FileOutputStream("karta.bin");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(frame.mapPanel.karta);
			oos.close();
			fos.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.exit(0);
	}


	public void windowClosed(WindowEvent e) {
		
	}

	public void windowIconified(WindowEvent e) {
		
	}

	public void windowDeiconified(WindowEvent e) {
	}


	public void windowActivated(WindowEvent e) {
		
	}

	public void windowDeactivated(WindowEvent e) {

	}
	
}
