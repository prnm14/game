
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameMapPanel extends Panel {
	
	private static final long serialVersionUID = 1L;
	Karta karta;
	int sizexInPixels;
	int sizeyInPixels;
	int bagX, bagY;

	int score = 0;

	int w, h;


	GameMapPanel(Karta k) {
		karta = k;
		sizexInPixels  = karta.sizex * 30;
		sizeyInPixels = karta.sizey * 30;

		w = karta.sizex;
		h = karta.sizey;

		bagX = (int) (Math.random()*(w - 1)+ 1);
		bagY = (int) (Math.random()*(h - 1) + 1);

		GameKeyListener gkl = new GameKeyListener(this);
		GameMouseListener gml = new GameMouseListener(this);
		addKeyListener(gkl);
		addMouseListener(gml);
	}
	
	public void paint(Graphics g) {
		System.out.println(karta.sizex);
		File f = new File("./hero.png");
		File bag = new File("./shopbag.png");
		File shop = new File("./shop.png");


		try {
			BufferedImage bi = ImageIO.read(f);
			BufferedImage bagimg = ImageIO.read(bag);
			BufferedImage shopimg = ImageIO.read(shop);
			g.drawImage(shopimg, 0, 0, 8 * 30, 8 * 30, null);
			g.drawImage(bi, karta.hero.posx * 30, karta.hero.posy * 30, 30, 30, null);
			g.drawImage(bagimg, bagX*30, bagY*30, 30, 30, null);

		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int i = 0; i <= w; i++) {
			g.drawLine(i * 30, 0, i * 30, sizeyInPixels);
		}
		for (int i = 0; i <= h; i++) {
			g.drawLine(0, i * 30, sizexInPixels, i * 30);
		}

		if(karta.hero.posx == bagX && karta.hero.posy == bagY) {
			bagX = (int) (Math.random() * w + 1);
			bagY = (int) (Math.random() * h + 1);
			score++;
			System.out.println("Rezultat: " + score);
		}
	}
}
