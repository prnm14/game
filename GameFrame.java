import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;

public class GameFrame extends Frame {
	private static final long serialVersionUID = 1L;
	GameMapPanel mapPanel;
	Label label;
	Button button;
	TextField textField;
	TextArea textArea;

	GameFrame(Karta k) {
		GameWindowListener gwl = new GameWindowListener(this);
		addWindowListener(gwl);
		
		mapPanel = new GameMapPanel(k);
		mapPanel.setBounds(0, 0, 301, 301);
		this.add(mapPanel);
		
		button = new Button();
		button.setBounds(k.sizex*30 + 60, 30, 100, 30);
		button.setLabel("������� ������");
		this.add(button);
		
		GameButtonActionListener al = new GameButtonActionListener(this);
		button.addActionListener(al);
		
		label = new Label();
		label.setBounds(k.sizex*30 + 60, 90, 100, 30);
		label.setText("������� ���� ��:");
		this.add(label);
		
		textField = new TextField();
		textField.setBounds(k.sizex*30 + 60, 150, 200, 30);
		textField.setText("�������� ����� �� ������");
		this.add(textField);
		
		textArea = new TextArea();
		textArea.setBounds(k.sizex*30 + 60, 210, 200, 110);
		textArea.setText("��� ��� ��� � ���, ����� � ������ �� ���� �� �� ������ ���������� � �����, ����� � �������� �� ����");
		this.add(textArea);
	}
