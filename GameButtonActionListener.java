
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameButtonActionListener implements ActionListener {
	GameFrame frame;
	
	GameButtonActionListener(GameFrame f) {
		frame = f;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String s = frame.textField.getText();
		frame.label.setText(s);
	}
}